@extends('site.layouts.layout')
@section('title',' صورة الفورمة ')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@section('content')
<!-- section begin -->
<section id="section-banner">
    <div class="container">
        <div class="content-img-banner">
            <div class="row align-items-center">
                <a class="width100" data-fancybox="gallery" href="{{asset('site/images/assets/banner1.png')}}">
                    <img src="{{asset('site/images/assets/banner1.png')}}" alt="">
                    <!-- if another page -->
                   <!--  <img src="{{asset('site/images/assets/banner2.png')}}" alt=""> -->
                </a>

{{--                <a data-fancybox="gallery" href="{{getImgPath(\App\Models\Setting::whereName('form_image')->first()->value)}}">--}}
{{--                    <img src="{{getImgPath(\App\Models\Setting::whereName('form_image')->first()->value)}}" alt="">--}}
{{--                </a>--}}
            </div>
            <div class="align-self-center ml-auto aligne-center mt15 header-col-right" style="background-size: cover;">
                <a class="btn-custom"  href="{{url('form')}}"><i class="fa fa-arrow-left"></i> دخول</a>
                <span id="menu-btn"></span>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

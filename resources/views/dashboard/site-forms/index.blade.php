@extends('dashboard.layouts.layout')
@section('title','الفورمة ')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">

                <div class="breadcrumb">
                    <a href="{{route('admin.main')}}" class="header-title m-t-0 m-b-30"><i class="icon-home2 mr-2"></i>
                        الرئيسية</a> /
                    <span class="breadcrumb-item active">@yield('title')</span>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            @include('dashboard.layouts.status')

                            <table id="datatable-keytable" class="table table-striped table-bordered text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الاسم</th>
                                    <th>البريد الالكترونى</th>
                                    <th>الجوال</th>
                                    <th>رقم الهوية</th>
                                    <th>بلد الجنسية</th>
                                    <th>بلد الاقامة</th>
                                    <th>المؤهل الدراسى</th>
                                    <th>التخصص الجامعى</th>
                                    <th>العمل الحالى</th>
                                    <th>الوصف</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($data as $index => $d)
                                    <tr>
                                        <td>{{$index +1}}</td>
                                        <td>{{$d->name}}</td>
                                        <td>{{$d->email}}</td>
                                        <td>{{$d->phone}}</td>
                                        <td>{{$d->identity}}</td>
                                        <td>{{$d->nationality}}</td>
                                        <td>{{$d->country}}</td>
                                        <td>{{$d->qualification}}</td>
                                        <td>{{$d->specialization}}</td>
                                        <td>{{$d->job}}</td>
                                        <td>{{$d->description}}</td>

                                        <td class="text-center">
                                            <button data-url="{{route('admin.site-forms.destroy',$d->id)}}"
                                                    class="btn-icon waves-effect btn btn-danger rounded-circle btn-sm ml-2 delete"
                                                    title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>
        </div><!-- end col -->
    </div>

@endsection

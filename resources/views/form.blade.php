@extends('site.layouts.layout')
@section('title',' اسم الفورمة')

@section('content')
    <!-- section begin -->
    <section id="subheader" data-bgimage="url(site/images/background/5.png) bottom">
        <div class="center-y relative text-center" data-scroll-speed="4">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 m-auto">
                        {{-- <form action='blank.php' class="row" id='form_subscribe' method="post" name="myForm">--}}
                        <div class="col-md-12 text-center">
                            <h1>التسجيل</h1>
                            <p>سجل الان من خلال الموقع بالمجان</p>
                        </div>
                        <div class="clearfix"></div>
                        {{-- </form>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->


    <section class="no-top" data-bgimage="url(site/images/background/3.png) top">
        <div class="container">
            {{-- @include('dashboard.layouts.status')--}}
            <div class="row">
                <div class="col-md-8 m-auto mb-sm-30">
                    <form name="contactForm " id="signup_form" class="form-border site_form" action="{{route('site.send_form')}}"
                          method="post">
                        @csrf

                        {{--/////////////////////////////////////////////////////////--}}
                        <div class="msg" style="display: none;">
                            <div class="alert alert-success alert-dismissable  " role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <p class="m-0 text-center">تم ارسال رسالتك بنجاح</p>
                            </div>
                        </div>
                        <p class="error1 text-center" style="margin-right: 100px;margin-left: 100px;"></p>
                        {{--/////////////////////////////////////////////////////////--}}

                        <div class="field-set">
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}"
                                   placeholder=" اسم المتقدم"/>
                        </div>


                        <div class="field-set">
                            <input type="text" name="identity" id="identity" value="{{old('identity')}}"
                                   class="form-control {{$errors->has('identity') ? ' is-invalid' : null}}"
                                   placeholder="رقم الهوية"/>
                        </div>


                        <div class="field-set">
                            <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control " placeholder="رقم الجوال"/>

                        </div>

                        <div class="field-set">
                            <input type="text" name="email" id="email" class="form-control " value="{{old('email')}}"
                                   placeholder="البريد الإلكتروني"/>
                        </div>
                        <div class="field-set">
                            <input type="text" name="nationality" id="nationality" value="{{old('nationality')}}"
                                   class="form-control {{$errors->has('nationality') ? ' is-invalid' : null}}"
                                   placeholder=" بلد الجنسية"/>
                        </div>
                        <div class="field-set">

                            <input type="text" name="country" id="country" value="{{old('country')}}"
                                   class="form-control {{$errors->has('country') ? ' is-invalid' : null}}"
                                   placeholder="بلد الاقامة "/>
                        </div>


                        <div class="field-set">

                            <input type="text" name="qualification" id="qualification" value="{{old('qualification')}}"
                                   class="form-control {{$errors->has('qualification') ? ' is-invalid' : null}}"
                                   placeholder=" المؤهل الدراسي "/>
                        </div>
                        <div class="field-set">
                            <input type="text" name="specialization" id="specialization" value="{{old('specialization')}}"
                                   class="form-control {{$errors->has('specialization') ? ' is-invalid' : null}}"
                                   placeholder=" التخصص الجامعي "/>
                        </div>
                        <div class="field-set full-width paddingLR">
                            <input type="text" name="job" id="job" value="{{old('job')}}"
                                   class="form-control {{$errors->has('job') ? ' is-invalid' : null}}"
                                   placeholder="  العمل الحالي "/>
                        </div>
                        <div class="field-set full-width paddingLR">
                            <textarea name="description" id="message" class="form-control"
                                      placeholder="وصف لنشاط شركة رائد الاعمال المزمع انشائها">{{old('description')}}</textarea>
                        </div>
                        <div class="spacer-half"></div>

                        <div id="submit">
                            <input type="submit" id="send_form_content" value="تسجيل " class="btn btn-custom"/>
                        </div>

                    </form>
                </div>
                <div class="spacer-double"></div>
            </div>
        </div>
    </section>
@endsection


@push('my-js')
    <script>
        $('#send_form_content').on('click', function (e) {
            e.preventDefault();
            var formData = new FormData($('.site_form')[0]);

            $.ajax({
                type: 'POST',
                url: "/form",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    if (data.status === true) {
                        $('.msg').removeAttr('style');
                        $('.error1').css('display', 'none');
                        $('.site_form')[0].reset();
                    } else {
                        let errors = $('.error1').addClass('alert alert-danger');
                        errors.empty()
                        errors.append(data.errors)
                    }
                },
                error(error) {
                    console.log('error', error);
                }

            });
        });

    </script>
@endpush

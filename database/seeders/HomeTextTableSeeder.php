<?php

namespace Database\Seeders;

use App\Models\HomeText;
use Illuminate\Database\Seeder;

class HomeTextTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'التكامل',
            'التمكين',
            'الابتكار',
        ];

        foreach ($names as $name) {
            HomeText::create(['name' => $name]);
        }
    }
}

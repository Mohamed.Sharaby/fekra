<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\GuestMessage;
use App\Models\MailList;
use App\Models\Partner;
use App\Models\Service;
use App\Models\Setting;
use App\Models\SiteForm;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $testimonials = Testimonial::all();
        $services = Service::active()->get();
        $partners = Partner::all();
        $blogs = Blog::active()->latest()->limit(3)->get();
        return view('site.index', compact('testimonials', 'services', 'partners', 'blogs'));
    }

    public function about()
    {
        $partners = Partner::all();
        return view('site.about', compact('partners'));
    }

    public function services()
    {
        $services = Service::active()->get();
        return view('site.services', compact('services'));
    }

    public function blog()
    {
        $blogs = Blog::active()->latest()->get();
        return view('site.blog', compact('blogs'));
    }

    public function singleBlog($id)
    {
        $blog = Blog::active()->findOrFail($id);
        $latestBlogs = Blog::latest()->limit(4)->get();
        return view('site.single-blog', compact('blog','latestBlogs'));
    }


    public function contact()
    {
        $address = Setting::whereName('address')->first();
        $location = Setting::whereName('location')->first();
        $phone = Setting::whereName('phone')->first();
        $mobile = Setting::whereName('mobile')->first();
        $email = Setting::whereName('email')->first();
        return view('site.contact', compact('address', 'location', 'mobile', 'email'));
    }


    public function PostContact(Request $request)
    {
        $vL = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|email',
            'phone' => 'required',
            'content' => 'required'
        ]);

        if ($vL->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $vL->errors()->first()
            ]);
        } else {
            GuestMessage::create($request->all());
            return response()->json([
                'status' => true,
            ]);
        }
    }


    public function subscribe(Request $request)
    {
        $vL = Validator::make($request->all(), [
            'email' => 'required|email|unique:mail_lists,email',
        ]);

        if ($vL->fails()) return response()->json([
            'status' => false,
            'errors' => $vL->errors()->first()
        ]);
        MailList::create($request->all());
        return response()->json([
            'status' => true,
        ]);
    }

    public function form()
    {

        return view('form');
    }

    public function page_image()
    {

        return view('page_image');
    }

    public function alhadina()
    {

        return view('alhadina');
    }


    public function sendForm(Request $request)
    {
        // dd($request->all());
        $vL = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|email',
            'phone' => 'required',
            'identity' => 'required',
            "nationality" => 'required',
            "country" =>'required',
            "qualification" =>'required',
            "specialization" => 'required',
            "job" => 'required',
            "description" => 'required',
        ]);

        if ($vL->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $vL->errors()->first()
            ]);
        } else {
            SiteForm::create($request->all());
            return response()->json([
                'status' => true,
            ]);
        }
    }

}

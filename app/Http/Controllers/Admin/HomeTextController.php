<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomeText;
use Illuminate\Http\Request;

class HomeTextController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Settings');
    }


    public function index()
    {
        $texts = HomeText::latest()->get();
        return view('dashboard.home-text.index', compact('texts'));
    }


    public function create()
    {
        return view('dashboard.home-text.create');
    }


    public function store(Request $request)
    {
        $validator = $request->validate(['name' => 'required']);
        HomeText::create($validator);
        return redirect(route('admin.home-text.index'))->with('success', 'تم الاضافة بنجاح');
    }


    public function destroy($id)
    {
        $text = HomeText::findOrFail($id);
        $text->delete();
        return 'Done';
    }
}

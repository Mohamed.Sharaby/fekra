<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GuestMessage;
use App\Models\SiteForm;

class SiteFormController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('permission:GuestMessages');
//    }



    public function index()
    {
        $data = SiteForm::all();
        return view('dashboard.site-forms.index', compact('data'));
    }


    public function destroy($id)
    {
        $data = SiteForm::findOrFail($id);
        $data->delete();
        return 'Done';
    }
}
